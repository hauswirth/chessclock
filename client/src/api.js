import openSocket from 'socket.io-client';
const url = window.location.protocol+'//'+window.location.hostname+':8000';
console.log(url);
const socket = openSocket(url);


function subscribe(updateCb, counterFinishedCb, finishedCb) {
  socket.on('update', state => updateCb(null, state));
  socket.on('counterFinished', counter => counterFinishedCb(null, counter));
  socket.on('finished', () => finishedCb(null, null));
  socket.emit('subscribe');
}

function unsubscribe() {
  socket.emit('unsubscribe');
}

function start() {
  socket.emit('start');
}

function pause() {
  socket.emit('pause');
}

function resume() {
  socket.emit('resume');
}

function end() {
  socket.emit('end');
}

function reset() {
  socket.emit('reset');
}

function switchCounter() {
  socket.emit('switch');
}

function setDuration(counter, duration) {
  socket.emit('setDuration', counter, duration);
}

export { subscribe, unsubscribe, start, pause, resume, end, reset, switchCounter, setDuration }
