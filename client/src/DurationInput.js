import React, { Component } from 'react';
import { renderSec } from './utils.js'


class DurationInput extends Component {
    state = {
        value: renderSec(this.props.duration),
        valid: true,
        seconds: this.props.duration
    };
    computeState = (value) => {
        console.log("computeState");
        const regex = /^([0-9]+):([0-9][0-9])$/;
        const match = regex.exec(value);
        console.log(match);
        if (match!=null) {
            const min = +match[1];
            const sec = +match[2];
            const seconds = sec+min*60;
            return {
                value: value,
                valid: true,
                seconds: seconds
            };
        } else {
            return {
                value: value,
                valid: false,
                seconds: undefined
            };
        }
    };
    handleChange = (ev) => {
        console.log("handleChange");
        this.setState(this.computeState(ev.target.value));
    };
    handleBlur = (ev) => {
        console.log("handleBlur");
        if (this.state.valid) {
            this.props.onValueChange(this.state.seconds);
        }
    };
    render() {
        return (
            <input
                style={this.state.valid?{}:{backgroundColor: 'red'}}
                type="text"
                value={this.state.value}
                onChange={this.handleChange}
                onBlur={this.handleBlur}
            />
        );
    }
}

export default DurationInput;
