import React, { Component } from 'react';
import './App.css';
import DurationInput from './DurationInput';
import { subscribe, start, pause, resume, end, reset, switchCounter, setDuration } from './api';
import { toSecString, toMinString } from './utils.js'

const finishedAudio = 'applauses.mp3';
const presentationFinishedAudio = 'wake-up-will-you-half.mov';
const qaFinishedAudio = 'wake-up-will-you-half.mov';

class App extends Component {
  constructor(props) {
    super(props);
    subscribe(
      (err, state) => this.handleUpdate(state),
      (err, counter) => this.handleCounterFinished(counter),
      (err) => this.handleFinished()
    );
  }
  state = {
    durations: [0, 0],
    counterValues: [0, 0],
    counterId: 0,
    state: 'waiting',
    // presentation only:
    size: 30 //TODO: provide slider to adjust, use in style tag
  };
  handleStart = () => {
    console.log("start");
    start();
  };
  handlePause = () => {
    console.log("pause");
    pause();
  };
  handleResume = () => {
    console.log("resume");
    resume();
  };
  handleEnd = () => {
    console.log("end");
    end();
  };
  handleReset = () => {
    console.log("reset");
    reset();
  };
  handleSwitchCounter = () => {
    console.log("switchCounter");
    switchCounter();
  };
  handleDurationChange = (counter, duration) => {
    console.log("handleDurationChange("+counter+","+duration+")")
    setDuration(counter, duration);
  };
  handleUpdate = (state) => {
    console.log("handleUpdate: ", state);
    this.setState({
      durations: state.durations,
      counterValues: state.counterValues,
      counterId: state.counterId,
      state: state.state,
    })
  };
  handleCounterFinished = (counter) => {
    console.log("handleCounterFinished: ", counter);
    let audio = null;
    if (counter===0) {
      // presentation
      audio = new Audio(presentationFinishedAudio);
    } else {
      // q&a
      audio = new Audio(qaFinishedAudio);
    }
    var promise = audio.play();
    if (promise !== undefined) {
      promise.then(_ => {
        // Autoplay started!
        console.log("play succeeded");
      }).catch(error => {
        // Autoplay was prevented.
        console.log("play prevented");
        // Would need to show a "Play" button 
        // so that user can start playback.
        // Infeasible for our kind of app...
      });
    }
  };
  handleFinished = () => {
    console.log("handleFinished");
    var audio = new Audio(finishedAudio);
    var promise = audio.play();
    if (promise !== undefined) {
      promise.then(_ => {
        // Autoplay started!
        console.log("play succeeded");
      }).catch(error => {
        // Autoplay was prevented.
        console.log("play prevented");
        // Would need to show a "Play" button 
        // so that user can start playback.
        // Infeasible for our kind of app...
      });
    }
  }
  render() {
    const getRemainingTime = (c) => {
      return this.state.durations[c]-this.state.counterValues[c];
    }
    const isDone = (c) => {
      return getRemainingTime(c)<=0;
    }
    const getClass = (c, suffix) => {
      return isDone(c) ? ("done"+suffix) : (c===this.state.counterId?("active"+suffix):("inactive"+suffix));
    }
    const buttons = (
      <span>
        {
          this.state.state==='ready' ?
            <button className='start button' onClick={this.handleStart}>Start</button> 
          :
            this.state.state==='running' ?
              <button className='pause button' onClick={this.handlePause}>Pause</button>
            :
              this.state.state==='paused' ?
                <button className='resume button' onClick={this.handleResume}>Resume</button>
              :
                <button className='disabled button'>&nbsp;</button>
        }
        <button className='switch button' onClick={this.handleSwitchCounter}>Switch</button>
        {
          this.state.state==='ended' ?
            <button className='reset button' onClick={this.handleReset}>Reset</button>
          :
            <button className='end button' onClick={this.handleEnd}>End</button>
        }
      </span>
    );
    return (
      <div className="App">
        <table width="100%">
          <tbody>
            <tr>
              <td style={{width: '48%'}} className={getClass(0, "Header")}>Presentation</td>
              <td style={{width: '4%'}}></td>
              <td style={{width: '48%'}} className={getClass(1, "Header")}>Q &amp; A</td>
            </tr>
            <tr>
              <td className={getClass(0, "Value")}>
                {toMinString(getRemainingTime(0))}
                <span style={{fontSize: "50%"}}>{":"+toSecString(getRemainingTime(0))}</span>
              </td>
              <td></td>
              <td className={getClass(1, "Value")}>
                {toMinString(getRemainingTime(1))}
                <span style={{fontSize: "50%"}}>{":"+toSecString(getRemainingTime(1))}</span>
              </td>
            </tr>
            <tr>
              <td colSpan="3" className="stateCell">
                {this.state.state}
              </td>
            </tr>
            <tr>
              <td colSpan="3" className="stateCell">
                {buttons}
              </td>
            </tr>
            <tr>
              <td>
                Presentation Duration: &nbsp;
                <DurationInput 
                  key={this.state.durations[0]} 
                  duration={this.state.durations[0]}
                  onValueChange={(v) => this.handleDurationChange(0, v)}
                />
              </td>
              <td>
              </td>
              <td>
                Q &amp; A Duration: &nbsp;
                <DurationInput 
                    key={this.state.durations[1]} 
                    duration={this.state.durations[1]}
                    onValueChange={(v) => this.handleDurationChange(1, v)}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default App;
