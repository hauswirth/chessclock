const renderSec = (v) => {
    console.log("renderSec("+v+")");
    const min = Math.floor(v/60);
    const sec = v%60;
    return min+":"+(sec<10?"0":"")+sec;
};

const toSecString = (duration) => {
    console.log("toSecString("+duration+")");
    const sec = duration%60;
    return (sec<10?"0":"")+sec;
}

const toMinString = (duration) => {
    console.log("toMinString("+duration+")");
    return Math.floor(duration/60);
}

export { renderSec, toSecString, toMinString };
