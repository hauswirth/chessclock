const io = require('socket.io')();

const state = {
  durations: [20*60, 10*60]
};
resetState();

const subscribedClients = [];

const interval = 1000; // ms
var timerId = undefined;

function startTimer() {
  console.log("startTimer()");
  if (!timerId) {
    timerId = setInterval(()=>{
      tick();
    }, interval);
  }
}

function stopTimer() {
  console.log("stopTimer()");
  if (timerId) {
    clearInterval(timerId);
    timerId = undefined;
  }
}

function tick() {
  console.log("tick");
  const counterId = state.counterId;
  if (state.counterValues[counterId]<state.durations[counterId]) {
    state.counterValues[counterId]++;
  } else {
    const otherCounterId = counterId==0?1:0;
    if (state.counterValues[otherCounterId]<state.durations[otherCounterId]) {
      notifyClientsCounterFinished(counterId);
      switchCounter();
      state.counterValues[otherCounterId]++;
    } else {
      notifyClientsFinished();
      end();
    }
  }
  updateClients();
}

function switchCounter() {
  state.counterId = state.counterId==0?1:0;
}

function end() {
  state.state = 'ended';
  stopTimer();
  updateClients();
}

function setDuration(counter, duration) {
  console.log("setDuration");
  state.durations[counter] = duration;
}

function resetState() {
  state.counterId = 0;
  state.counterValues = [0, 0];
  state.state = "ready";
}

function updateClients() {
  subscribedClients.forEach((v,i,a) => {
    v.emit('update', state);
  });
}

function notifyClientsCounterFinished(counter) {
  subscribedClients.forEach((v,i,a) => {
    v.emit('counterFinished', counter);
  });
}

function notifyClientsFinished() {
  subscribedClients.forEach((v,i,a) => {
    v.emit('finished', state);
  });
}

io.on('connection', (client) => {
  client.on('subscribe', () => {
    console.log('subscribe client:', client);
    subscribedClients.push(client);
    updateClients();
  });
  client.on('unsubscribe', () => {
    console.log('unsubscribe client:', client);
    var index = subscribedClients.indexOf(client);
    if (index > -1) {
      subscribedClients.splice(index, 1);
    }
  });
  client.on('start', () => {
    console.log('start');
    state.state = 'running';
    startTimer();
    updateClients()
  });
  client.on('pause', () => {
    console.log('pause');
    state.state = 'paused';
    stopTimer();
    updateClients()
  });
  client.on('resume', () => {
    console.log('resume');
    state.state = 'running';
    startTimer();
    updateClients()
  });
  client.on('end', () => {
    console.log('end');
    end();
  });
  client.on('reset', () => {
    console.log('reset');
    stopTimer();
    resetState();
    updateClients()
  });
  client.on('switch', () => {
    console.log('switch');
    switchCounter();
    updateClients()
  });
  client.on('setDuration', (counter, duration) => {
    console.log('setDuration');
    setDuration(counter, duration);
    updateClients()
  });
});

const port = 8000;
io.listen(port);
console.log('listening on port ', port);
