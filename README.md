# Chess Clock Talk Timer

This is a web-based application for timing talks 
where the audience is encouraged to interact with the presenter.
The timer allows to separately allocate time for the presentation and for the discussion/Q&A.
For example, one can allocate 25 minutes for the presentation and 25 minutes for discussion.
The goal of using this timer is to allow presentation and discussion to be interleaved.
A (human) time keeper (the "session chair", or any other audience member) sits at the timer
and switches between presentation and discussion times whenever needed.

The application is based on how the 
[IFIP Working Group on Language Design](http://program-transformation.org/WGLD/Meeting2018) 
conducts their meetings:

> The program consists of a series of 50 minute sessions, which are scheduled during the
> meeting. In each session the speaker gets 25 minutes to present and the audience gets 
> 25 minutes to discuss, in no particular order (although we tend to give the presenter 
> a couple of minutes to at least introduce the topic). 
> Time is administered using a chess clock.

This chess clock timer application allows setting different presentation and discussion times.

An arbitrary number of clients can connect to the app.
Usually one client is a computer or tablet placed in front of the room 
for the whole audience to see, and another client is placed so the speaker can see it.
A third client (or one of the others) can be used by a human timekeeper
to switch between presentation and discussion times (clicking the timer's "switch" button).
Usually, the discussion time includes the time the presenter needs to answer questions.
This way, the presenter is guaranteed to have enough time to present all their contents,
and the audience is guaranteed ample time to 


## Usage
This repository contains both the client (a react web app) and the server (a node app).

To run the server:

```
cd server; yarn install; yarn start
```

To run the client (i.e., to spin up a web server that serves the react app):

```
cd client; yarn install; yarn start
```

Note that the above commands run the app in developer mode.
This usually is fine.
The server sends a message to each connected client once every second.

The clients and the server need to communicate over web sockets,
and thus need to be connected to a network that doesn't block them.
Experience shows that this may not work in some hotel wifi networks.
If you want to use this for a conference, 
bring a base station or use your smart phone as a wifi base station
to work around this.


## Implementation

The implementation uses socket.io to communicate between clients and server.
It was heavily inspired by Hendrik Swanepoel's Medium post
[Combining React with Socket.io for real-time goodness](https://medium.com/dailyjs/combining-react-with-socket-io-for-real-time-goodness-d26168429a34).

The user interface was inspired by this 
[online chess timer](https://www.timeme.com/chess-timer.htm).

The included notification sounds are licensed under the 
[Creative Commons Attribution license](https://creativecommons.org/licenses/by/4.0/legalcode)
from https://notificationsounds.com/.
